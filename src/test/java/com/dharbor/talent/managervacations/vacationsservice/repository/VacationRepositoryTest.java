package com.dharbor.talent.managervacations.vacationsservice.repository;

import com.dharbor.talent.managervacations.vacationsservice.constant.VacationStatus;
import com.dharbor.talent.managervacations.vacationsservice.constant.VacationType;
import com.dharbor.talent.managervacations.vacationsservice.domain.Vacation;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ActiveProfiles("test")
class VacationRepositoryTest {

    @Autowired
    private VacationRepository undertest;

    @Test
    void existsByDayOffAndUserId() {
        Vacation vacation = new Vacation();
        vacation.setVacationStatus(VacationStatus.APPROVED);
        vacation.setDayOff(new Date());
        vacation.setUserId(1L);
        vacation.setVacationType(VacationType.HOLIDAY);
        undertest.save(vacation);

        Boolean result = undertest.existsByDayOffAndUserId(vacation.getDayOff(), vacation.getUserId());
        assertThat(result).isTrue();
    }

    @Test
    void existsByDayOffAndUserIdNotFound() {
        Vacation vacation = new Vacation();
        vacation.setVacationStatus(VacationStatus.APPROVED);
        vacation.setDayOff(new Date());
        vacation.setUserId(1L);
        vacation.setVacationType(VacationType.HOLIDAY);

        Boolean result = undertest.existsByDayOffAndUserId(vacation.getDayOff(), vacation.getUserId());
        assertThat(result).isFalse();
    }

    @Test
    void countByUserIdAndVacationStatus() {
        Vacation vacation = new Vacation();
        vacation.setVacationStatus(VacationStatus.APPROVED);
        vacation.setDayOff(new Date());
        vacation.setUserId(1L);
        vacation.setVacationType(VacationType.HOLIDAY);
        undertest.save(vacation);

        int result = undertest.countByUserIdAndVacationStatus(vacation.getId(), vacation.getVacationStatus());
        assertThat(result==1).isTrue();
    }

    @Test
    void countByUserIdAndVacationStatusNotFound() {
        Vacation vacation = new Vacation();
        vacation.setVacationStatus(VacationStatus.APPROVED);
        vacation.setDayOff(new Date());
        vacation.setUserId(1L);
        vacation.setVacationType(VacationType.HOLIDAY);

        int result = undertest.countByUserIdAndVacationStatus(vacation.getId(), vacation.getVacationStatus());
        assertThat(result==1).isFalse();
    }
}
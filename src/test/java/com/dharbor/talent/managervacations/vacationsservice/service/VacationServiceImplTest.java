package com.dharbor.talent.managervacations.vacationsservice.service;

import com.dharbor.talent.managervacations.vacationsservice.common.Message;
import com.dharbor.talent.managervacations.vacationsservice.constant.VacationStatus;
import com.dharbor.talent.managervacations.vacationsservice.constant.VacationType;
import com.dharbor.talent.managervacations.vacationsservice.domain.Vacation;
import com.dharbor.talent.managervacations.vacationsservice.repository.VacationRepository;
import com.dharbor.talent.managervacations.vacationsservice.validator.Verifications;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class VacationServiceImplTest {

    @Mock
    private VacationRepository vacationRepository;

    private Verifications verifications;

    private Message message;

    private VacationServiceImpl undertest;

    @BeforeEach
    void setUp(){
       verifications = new Verifications();
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        message = new Message(resourceBundleMessageSource);
        undertest = new VacationServiceImpl(vacationRepository, verifications, message);
    }

    @Test
    void save() {
        Vacation vacation = new Vacation();
        vacation.setVacationStatus(VacationStatus.APPROVED);
        vacation.setDayOff(new Date());
        vacation.setUserId(1L);
        vacation.setVacationType(VacationType.HOLIDAY);

        Vacation vacationSaved = new Vacation();
        vacationSaved.setVacationStatus(vacation.getVacationStatus());
        vacationSaved.setDayOff(vacation.getDayOff());
        vacationSaved.setUserId(vacation.getUserId());
        vacationSaved.setVacationType(vacation.getVacationType());
        vacationSaved.setId(1L);

        Mockito.when(vacationRepository.save(vacation)).thenReturn(vacationSaved);
        Vacation result = undertest.save(vacation);

        verify(vacationRepository, times(1)).save(vacation);
        assertTrue(result.getId().compareTo(1L) == 0);
    }

    @Test
    void countByUserIdAndVacationStatus() {
        Vacation vacation = new Vacation();
        vacation.setVacationStatus(VacationStatus.APPROVED);
        vacation.setDayOff(new Date());
        vacation.setUserId(1L);
        vacation.setVacationType(VacationType.HOLIDAY);

        Mockito.when(vacationRepository.countByUserIdAndVacationStatus(vacation.getUserId(), vacation.getVacationStatus())).thenReturn(1);
        int result = undertest.countByUserIdAndVacationStatus(vacation.getUserId(), vacation.getVacationStatus());

        verify(vacationRepository).countByUserIdAndVacationStatus(vacation.getUserId(), vacation.getVacationStatus());
        assertTrue(result == 1);
    }

    @Test
    void findById() {
        Vacation vacation = new Vacation();
        vacation.setVacationStatus(VacationStatus.APPROVED);
        vacation.setDayOff(new Date());
        vacation.setUserId(1L);
        vacation.setVacationType(VacationType.HOLIDAY);

        Mockito.when(vacationRepository.findById(vacation.getId())).thenReturn(Optional.of(vacation));
        Optional<Vacation> result = undertest.findById(vacation.getId());

        verify(vacationRepository).findById(vacation.getId());
        Assertions.assertEquals(true, result.isPresent());
    }

    @Test
    void existsByDayOffAndUserId() {
        Vacation vacation = new Vacation();
        vacation.setVacationStatus(VacationStatus.APPROVED);
        vacation.setDayOff(new Date());
        vacation.setUserId(1L);
        vacation.setVacationType(VacationType.HOLIDAY);

        Mockito.when(vacationRepository.existsByDayOffAndUserId(vacation.getDayOff(), vacation.getUserId())).thenReturn(true);
        Boolean result = undertest.existsByDayOffAndUserId(vacation.getDayOff(), vacation.getUserId());

        verify(vacationRepository).existsByDayOffAndUserId(vacation.getDayOff(), vacation.getUserId());
        Assertions.assertTrue(result);
    }
}
package com.dharbor.talent.managervacations.vacationsservice.validator;

import com.dharbor.talent.managervacations.vacationsservice.exception.BadRequestException;

/**
 * @author Jhonatan Soto
 */
public class VerifyExistDayOffSameDayCommand implements Command {

    private final String message;
    private final boolean validate;

    public VerifyExistDayOffSameDayCommand(String message, boolean validate) {
        this.message = message;
        this.validate = validate;
    }

    @Override
    public void execute() {
        if (validate) {
            throw new BadRequestException(message);
        }
    }
}

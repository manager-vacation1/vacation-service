package com.dharbor.talent.managervacations.vacationsservice.validator;

import com.dharbor.talent.managervacations.vacationsservice.constant.UserType;
import com.dharbor.talent.managervacations.vacationsservice.constant.VacationType;
import com.dharbor.talent.managervacations.vacationsservice.exception.BadRequestException;

/**
 * @author Jhonatan Soto
 */
public class VerifyEnumUserType implements Command {
    private final boolean validate;
    private final String message;

    public VerifyEnumUserType(VacationType vacationType, String message) {
        this.validate = VacationType.findByType(vacationType);
        this.message = message;
    }

    @Override
    public void execute() {
        if (!validate) {
            throw new BadRequestException(message);
        }
    }
}

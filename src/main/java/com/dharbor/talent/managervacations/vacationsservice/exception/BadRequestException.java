package com.dharbor.talent.managervacations.vacationsservice.exception;

public class BadRequestException extends RuntimeException {
    public BadRequestException(String message){
        super(message);
    }
}

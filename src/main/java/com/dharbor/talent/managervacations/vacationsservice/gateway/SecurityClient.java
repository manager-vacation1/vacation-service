package com.dharbor.talent.managervacations.vacationsservice.gateway;

/**
 * @author Jhonatan Soto
 */

import com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.UserResponse;
import com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.profile.UserAndProfileResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@FeignClient("security-service")
@Service
public interface SecurityClient {
    @GetMapping("/user/{userId}")
    UserResponse getUserById(@PathVariable Long userId);

    @GetMapping("/profile/{userId}")
    UserAndProfileResponse getProfileByUserId(@PathVariable Long userId);

    @PutMapping("/profile/subtract/numberOfOffsets/{userId}")
    void subtractNumberOfOffsets(@PathVariable Long userId);

}

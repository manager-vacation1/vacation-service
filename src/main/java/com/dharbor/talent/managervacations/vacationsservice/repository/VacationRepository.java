package com.dharbor.talent.managervacations.vacationsservice.repository;

import com.dharbor.talent.managervacations.vacationsservice.constant.VacationStatus;
import com.dharbor.talent.managervacations.vacationsservice.domain.Vacation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Repository
public interface VacationRepository extends JpaRepository<Vacation, Long> {

    boolean existsByDayOffAndUserId(Date datOff, Long userId);

    Integer countByUserIdAndVacationStatus(Long id, VacationStatus vacationStatus);

    @Query(nativeQuery = true, value = "select * from vacations_table where month(vacations_day_off) = ?1 and vacations_user_id = ?2")
    List<Vacation> findByMonthAndCountryId(Integer m, Long id);
}

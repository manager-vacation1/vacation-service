package com.dharbor.talent.managervacations.vacationsservice.validator;

import com.dharbor.talent.managervacations.vacationsservice.exception.BadRequestException;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author Jhonatan Soto
 */
public class VerifyDateBeforeCommand implements Command {
    private final Date dateVacation;
    private final String message;

    public VerifyDateBeforeCommand(Date dateVacation, String message) {
        this.dateVacation = dateVacation;
        this.message = message;
    }

    @Override
    public void execute() {
        LocalDate localDate = LocalDate.now();
        Date dateNow = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        if (!dateNow.before(dateVacation)) {
            throw new BadRequestException(message);
        }
    }
}

package com.dharbor.talent.managervacations.vacationsservice.validator;

/**
 * @author Jhonatan Soto
 */
public interface Command {
    void execute();
}

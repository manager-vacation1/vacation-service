package com.dharbor.talent.managervacations.vacationsservice.mapper;

import com.dharbor.talent.managervacations.vacationsservice.domain.Vacation;
import com.dharbor.talent.managervacations.vacationsservice.dto.VacationDto;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Mapper(componentModel = "spring")
public interface VacationStructMapper {

    VacationDto vacationToVacationDtio(Vacation vacation);

    List<VacationDto> listVacationToListVacationDto(List<Vacation> listVacation);

}

package com.dharbor.talent.managervacations.vacationsservice.service;

import com.dharbor.talent.managervacations.vacationsservice.constant.VacationStatus;
import com.dharbor.talent.managervacations.vacationsservice.domain.Vacation;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
public interface IVacationService {
    Vacation save(Vacation vacation);

    Integer countByUserIdAndVacationStatus(Long id, VacationStatus vacationStatus);

    Optional<Vacation> findById(Long id);

    boolean existsByDayOffAndUserId(Date datOff, Long userId);

    List<Vacation> findByMonthAndCountryId(Integer m, Long id);

}

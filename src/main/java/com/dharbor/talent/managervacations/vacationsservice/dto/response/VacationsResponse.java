package com.dharbor.talent.managervacations.vacationsservice.dto.response;

import com.dharbor.talent.managervacations.vacationsservice.constant.ResponseConstant;
import com.dharbor.talent.managervacations.vacationsservice.domain.CommonResponseDomain;
import com.dharbor.talent.managervacations.vacationsservice.dto.VacationDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class VacationsResponse extends CommonResponseDomain {
    private List<VacationDto> vacationsDto;

    public VacationsResponse(List<VacationDto> vacationsDto) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.vacationsDto = vacationsDto;
    }
}

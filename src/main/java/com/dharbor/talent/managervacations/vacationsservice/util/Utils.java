package com.dharbor.talent.managervacations.vacationsservice.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public final class Utils {

    private Utils() {
        throw new IllegalStateException("Must not have constructor It is a utility class");
    }

    public static boolean isNull(Object value){
        return value == null;
    }

    public static LocalDateTime convertDateToLocalDate(Date date){
        return  LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }
}

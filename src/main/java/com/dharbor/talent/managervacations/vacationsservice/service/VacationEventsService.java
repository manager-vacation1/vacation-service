package com.dharbor.talent.managervacations.vacationsservice.service;

import com.dharbor.talent.managervacations.vacationsservice.dto.VacationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * @author Jhonatan Soto
 */
@Component
public class VacationEventsService {

    @Autowired
    private KafkaTemplate<String, VacationDto> producer;

    @Value("${topic.vacation.name:test3}")
    private String topicVacation;

    public void publish(VacationDto vacationDto) {
        producer.send(topicVacation, vacationDto);
    }
}

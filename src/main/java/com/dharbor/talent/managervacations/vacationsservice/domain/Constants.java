package com.dharbor.talent.managervacations.vacationsservice.domain;

/**
 * @author Josue Jimenez Nina
 */
final class Constants {

    static class VacationsTable {
        static final String NAME = "vacations_table";

        static class Id {
            static final String NAME = "vacations_id";
        }

        static class Type {
            static final String NAME = "vacations_type";
            static final int LENGTH = 12;
        }

        static class DayOff {
            static final String NAME = "vacations_day_off";
        }

        static class Status {
            static final String NAME = "vacations_status";
            static final int LENGTH = 9;
        }

        static class User {
            static final String NAME = "vacations_user_id";
        }
    }
}

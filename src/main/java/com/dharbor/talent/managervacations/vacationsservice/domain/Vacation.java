package com.dharbor.talent.managervacations.vacationsservice.domain;

import com.dharbor.talent.managervacations.vacationsservice.constant.VacationStatus;
import com.dharbor.talent.managervacations.vacationsservice.constant.VacationType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@Entity
@Table(name = Constants.VacationsTable.NAME)
public class Vacation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = Constants.VacationsTable.Type.NAME, length = Constants.VacationsTable.Type.LENGTH, nullable = false)
    private VacationType vacationType;

    @Temporal(TemporalType.DATE)
    @Column(name = Constants.VacationsTable.DayOff.NAME, nullable = false)
    private Date dayOff;

    @Enumerated(EnumType.STRING)
    @Column(name = Constants.VacationsTable.Status.NAME, length = Constants.VacationsTable.Status.LENGTH, nullable = false)
    private VacationStatus vacationStatus;

    @Column(name = Constants.VacationsTable.User.NAME, nullable = false)
    private Long userId;

    @PrePersist
    void onPrePersist() {
        this.vacationStatus = VacationStatus.REQUESTED;
    }

}

package com.dharbor.talent.managervacations.vacationsservice.gateway;

import com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.holiday.HolidayExistResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@FeignClient("holiday-service")
@Service
public interface VacationClient {
    @GetMapping("/date/country/")
    HolidayExistResponse getHolidaysByDateAndCountryId(
            @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date,@RequestParam("countryId") Long countryId);
}

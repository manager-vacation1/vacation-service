package com.dharbor.talent.managervacations.vacationsservice.controller;

import com.dharbor.talent.managervacations.vacationsservice.dto.request.VacationRequest;
import com.dharbor.talent.managervacations.vacationsservice.dto.request.VacationUpdateRequest;
import com.dharbor.talent.managervacations.vacationsservice.dto.response.VacationResponse;
import com.dharbor.talent.managervacations.vacationsservice.dto.response.VacationsResponse;
import com.dharbor.talent.managervacations.vacationsservice.usecase.CreateVacationUseCase;
import com.dharbor.talent.managervacations.vacationsservice.usecase.GetByVacationIdUseCase;
import com.dharbor.talent.managervacations.vacationsservice.usecase.GetVacationsByUserIdAndMonthUseCase;
import com.dharbor.talent.managervacations.vacationsservice.usecase.UpdateVacationStatusUseCase;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author Jhonatan Soto
 */
@RestController
@RequestMapping("/vacation")
@AllArgsConstructor
public class VacationController {

    private CreateVacationUseCase createVacationUseCase;

    private GetByVacationIdUseCase getByVacationIdUseCase;

    private UpdateVacationStatusUseCase updateVacationStatusUseCase;

    private GetVacationsByUserIdAndMonthUseCase getVacationsByUserIdAndMonthUseCase;

    @GetMapping("/{vacationId}")
    public VacationResponse getVacationById(@PathVariable Long vacationId) {
        return getByVacationIdUseCase.execute(vacationId);
    }

    @PutMapping("/update/status")
    public VacationResponse updateVacationStatus(@RequestBody VacationUpdateRequest vacationUpdateRequest) {
        return updateVacationStatusUseCase.execute(vacationUpdateRequest);
    }

    @PostMapping("/create/{userId}")
    public VacationResponse createVacation(@RequestBody VacationRequest vacationRequest, @PathVariable Long userId) {
        return createVacationUseCase.execute(vacationRequest, userId);
    }

    @GetMapping("/month/user")
    public VacationsResponse getHolidaysByMonthAndUserId(@RequestParam Integer month, Long userId) {
        return getVacationsByUserIdAndMonthUseCase.execute(month, userId);
    }
}

package com.dharbor.talent.managervacations.vacationsservice.dto.request;

import com.dharbor.talent.managervacations.vacationsservice.constant.VacationType;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class VacationRequest {

    private VacationType vacationType;

    private Date dayOff;

}

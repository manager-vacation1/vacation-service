package com.dharbor.talent.managervacations.vacationsservice.usecase;

import com.dharbor.talent.managervacations.vacationsservice.dto.VacationDto;
import com.dharbor.talent.managervacations.vacationsservice.dto.response.VacationResponse;
import com.dharbor.talent.managervacations.vacationsservice.mapper.VacationStructMapper;
import com.dharbor.talent.managervacations.vacationsservice.service.IVacationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class GetByVacationIdUseCase {

    private IVacationService iVacationService;

    private VacationStructMapper vacationStructMapper;

    public VacationResponse execute(Long id) {
        VacationDto vacationDto = vacationStructMapper.vacationToVacationDtio(iVacationService.findById(id).get());
        return new VacationResponse(vacationDto);
    }
}

package com.dharbor.talent.managervacations.vacationsservice.constant;

public enum UserType {
    MANAGER, COLLABORATOR;
}
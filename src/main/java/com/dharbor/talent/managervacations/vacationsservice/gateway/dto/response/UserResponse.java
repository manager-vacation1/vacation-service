package com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class UserResponse extends CommonResponseDto {
    private UserDto userDto;
}

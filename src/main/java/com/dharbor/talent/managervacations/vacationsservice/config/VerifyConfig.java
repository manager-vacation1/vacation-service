package com.dharbor.talent.managervacations.vacationsservice.config;

import com.dharbor.talent.managervacations.vacationsservice.validator.Verifications;
import org.springframework.cloud.openfeign.FeignFormatterRegistrar;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.Formatter;
import org.springframework.format.FormatterRegistry;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Jhonatan Soto
 */
@Configuration
public class VerifyConfig {
    @Bean
    public Verifications verifications() {
        return new Verifications();
    }

    @Component
    public static class DateFormatter implements Formatter<Date> {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

        @Override
        public Date parse(String text, Locale locale) throws ParseException {
            return formatter.parse(text);
        }

        @Override
        public String print(Date date, Locale locale) {
            return formatter.format(date);
        }

    }


    @Configuration
    public class FeignFormatterRegister implements FeignFormatterRegistrar {

        @Override
        public void registerFormatters(FormatterRegistry registry) {
            registry.addFormatter(new DateFormatter());
        }
    }
}

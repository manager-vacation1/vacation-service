package com.dharbor.talent.managervacations.vacationsservice.exception;

import com.dharbor.talent.managervacations.vacationsservice.constant.StatusCode;
import com.dharbor.talent.managervacations.vacationsservice.domain.CommonResponseDomain;
import feign.FeignException;
import org.hibernate.PropertyValueException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityNotFoundException;

/**
 * @author Jhonatan Soto
 */
@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<CommonResponseDomain> unknownException(Exception ex) {
        return new ResponseEntity<>(new CommonResponseDomain(StatusCode.INTERNAL_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {BadRequestException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<CommonResponseDomain> badRequestException(BadRequestException ex) {
        return new ResponseEntity<>(new CommonResponseDomain(StatusCode.BAD_REQUEST_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {EntityNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<CommonResponseDomain> entityNotFound(EntityNotFoundException ex) {
        return new ResponseEntity<>(new CommonResponseDomain(StatusCode.RESOURCE_NOT_FOUND_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {ResourceNotFound.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<CommonResponseDomain> resourceNotfound(ResourceNotFound ex) {
        return new ResponseEntity<>(new CommonResponseDomain(StatusCode.RESOURCE_NOT_FOUND_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {ServerErrorException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<CommonResponseDomain> serverError(ServerErrorException ex) {
        return new ResponseEntity<>(new CommonResponseDomain(StatusCode.INTERNAL_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {PropertyValueException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<CommonResponseDomain> notNullPropertyReference(PropertyValueException ex) {
        return new ResponseEntity<>(new CommonResponseDomain(StatusCode.INTERNAL_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {FeignException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<CommonResponseDomain> feignExceptionHandler(FeignException ex) {
        return new ResponseEntity<>(new CommonResponseDomain(StatusCode.INTERNAL_EXCEPTION.get(),
                ex.getMessage()),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

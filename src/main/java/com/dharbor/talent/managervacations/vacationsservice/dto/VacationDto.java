package com.dharbor.talent.managervacations.vacationsservice.dto;

import com.dharbor.talent.managervacations.vacationsservice.constant.VacationStatus;
import com.dharbor.talent.managervacations.vacationsservice.constant.VacationType;
import lombok.*;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class VacationDto {

    private Long id;

    private VacationType vacationType;

    private Date dayOff;

    private VacationStatus vacationStatus;

    private Long userId;
}

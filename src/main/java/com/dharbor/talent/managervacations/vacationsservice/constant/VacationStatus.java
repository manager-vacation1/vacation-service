package com.dharbor.talent.managervacations.vacationsservice.constant;

/**
 * @author Jhonatan Soto
 */
public enum VacationStatus {
    REQUESTED,
    APPROVED,
    DENIED
}

package com.dharbor.talent.managervacations.vacationsservice.validator;

import com.dharbor.talent.managervacations.vacationsservice.exception.BadRequestException;

/**
 * @author Jhonatan Soto
 */
public class VerifyIntegerNumbersCommand implements Command {
    private final boolean validate;
    private final String message;

    public VerifyIntegerNumbersCommand(Integer number, String message) {
        this.validate = number == null || number <= 0;
        this.message = message;
    }
    @Override
    public void execute() {
        if(validate){
            throw new BadRequestException(message);
        }
    }
}

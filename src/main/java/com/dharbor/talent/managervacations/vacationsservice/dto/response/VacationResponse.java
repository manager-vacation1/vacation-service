package com.dharbor.talent.managervacations.vacationsservice.dto.response;

import com.dharbor.talent.managervacations.vacationsservice.constant.ResponseConstant;
import com.dharbor.talent.managervacations.vacationsservice.domain.CommonResponseDomain;
import com.dharbor.talent.managervacations.vacationsservice.dto.VacationDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class VacationResponse extends CommonResponseDomain {
    private VacationDto vacationDto;

    public VacationResponse(VacationDto vacationDto) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.vacationDto = vacationDto;
    }
}

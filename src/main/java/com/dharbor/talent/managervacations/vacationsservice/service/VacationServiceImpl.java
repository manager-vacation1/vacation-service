package com.dharbor.talent.managervacations.vacationsservice.service;

import com.dharbor.talent.managervacations.vacationsservice.common.Message;
import com.dharbor.talent.managervacations.vacationsservice.constant.VacationStatus;
import com.dharbor.talent.managervacations.vacationsservice.domain.Vacation;
import com.dharbor.talent.managervacations.vacationsservice.exception.BadRequestException;
import com.dharbor.talent.managervacations.vacationsservice.repository.VacationRepository;
import com.dharbor.talent.managervacations.vacationsservice.validator.Verifications;
import com.dharbor.talent.managervacations.vacationsservice.validator.VerifyDateCommand;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class VacationServiceImpl implements IVacationService {

    private VacationRepository vacationRepository;

    private Verifications verifications;

    private Message message;

    @Override
    public Vacation save(Vacation vacation) {
        verifications.verify(new VerifyDateCommand(vacation.getDayOff(),
                message.getMessage("Not.Null.Vacation.Date")));
        return vacationRepository.save(vacation);
    }

    @Override
    public Integer countByUserIdAndVacationStatus(Long id, VacationStatus vacationStatus) {
        return vacationRepository.countByUserIdAndVacationStatus(id, vacationStatus);
    }

    @Override
    public Optional<Vacation> findById(Long id) {
        return Optional.ofNullable(vacationRepository.findById(id)
                .orElseThrow(() -> new BadRequestException(message.getMessage("Not.Exist.Vacation.id"))));
    }

    @Override
    public boolean existsByDayOffAndUserId(Date datOff, Long userId) {
        return vacationRepository.existsByDayOffAndUserId(datOff, userId);
    }

    @Override
    public List<Vacation> findByMonthAndCountryId(Integer m, Long id) {
        return vacationRepository.findByMonthAndCountryId(m, id);
    }

}

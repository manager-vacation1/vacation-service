package com.dharbor.talent.managervacations.vacationsservice.usecase;

import com.dharbor.talent.managervacations.vacationsservice.common.Message;
import com.dharbor.talent.managervacations.vacationsservice.exception.BadRequestException;
import com.dharbor.talent.managervacations.vacationsservice.exception.ResourceNotFound;
import com.dharbor.talent.managervacations.vacationsservice.exception.ServerErrorException;
import com.dharbor.talent.managervacations.vacationsservice.gateway.SecurityClient;
import com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.UserResponse;
import com.dharbor.talent.managervacations.vacationsservice.service.IVacationService;
import com.dharbor.talent.managervacations.vacationsservice.util.Utils;
import feign.FeignException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class VerifyHolidayUseCase {

    private IVacationService vacationService;

    private SecurityClient securityClient;

    private Message message;



}

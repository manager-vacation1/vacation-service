package com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CommonResponseDto {
    private String statusCode;
    private String message;
}

package com.dharbor.talent.managervacations.vacationsservice.usecase;

import com.dharbor.talent.managervacations.vacationsservice.domain.Vacation;
import com.dharbor.talent.managervacations.vacationsservice.dto.response.VacationsResponse;
import com.dharbor.talent.managervacations.vacationsservice.mapper.VacationStructMapper;
import com.dharbor.talent.managervacations.vacationsservice.service.IVacationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class GetVacationsByUserIdAndMonthUseCase {

    private IVacationService iVacationService;

    private VacationStructMapper vacationStructMapper;

    public VacationsResponse execute(Integer month, Long userId) {
        List<Vacation> list = iVacationService.findByMonthAndCountryId(month, userId);
        return new VacationsResponse(vacationStructMapper.listVacationToListVacationDto(list));
    }

}

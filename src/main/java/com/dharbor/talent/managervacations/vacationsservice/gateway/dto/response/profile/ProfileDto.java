package com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.profile;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class ProfileDto {

    private Long id;

    private String firstName;

    private String lastName;

    private Date birthdate;

    private String nickName;

    private Short numberOfOffsets;

    private String imageMongoId;

    private Long idCountry;

}

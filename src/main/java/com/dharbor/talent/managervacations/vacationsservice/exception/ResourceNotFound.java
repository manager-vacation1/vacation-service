package com.dharbor.talent.managervacations.vacationsservice.exception;

/**
 * @author Jhonatan Soto
 */
public class ResourceNotFound extends RuntimeException {
    public ResourceNotFound(String message) {
        super(message);
    }
}

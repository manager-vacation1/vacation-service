package com.dharbor.talent.managervacations.vacationsservice.validator;

/**
 * @author Jhonatan Soto
 */
public class Verifications {

    public void verify(Command command) {
        command.execute();
    }

}

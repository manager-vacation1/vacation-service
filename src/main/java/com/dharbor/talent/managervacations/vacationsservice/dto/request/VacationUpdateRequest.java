package com.dharbor.talent.managervacations.vacationsservice.dto.request;

import com.dharbor.talent.managervacations.vacationsservice.constant.VacationStatus;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class VacationUpdateRequest {

    private Long idVacation;

    private VacationStatus vacationStatus;

}

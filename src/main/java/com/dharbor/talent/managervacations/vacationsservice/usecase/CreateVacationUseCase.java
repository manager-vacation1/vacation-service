package com.dharbor.talent.managervacations.vacationsservice.usecase;

import com.dharbor.talent.managervacations.vacationsservice.common.Message;
import com.dharbor.talent.managervacations.vacationsservice.constant.VacationStatus;
import com.dharbor.talent.managervacations.vacationsservice.domain.Vacation;
import com.dharbor.talent.managervacations.vacationsservice.dto.VacationDto;
import com.dharbor.talent.managervacations.vacationsservice.dto.request.VacationRequest;
import com.dharbor.talent.managervacations.vacationsservice.dto.response.VacationResponse;
import com.dharbor.talent.managervacations.vacationsservice.exception.BadRequestException;
import com.dharbor.talent.managervacations.vacationsservice.exception.ResourceNotFound;
import com.dharbor.talent.managervacations.vacationsservice.exception.ServerErrorException;
import com.dharbor.talent.managervacations.vacationsservice.gateway.SecurityClient;
import com.dharbor.talent.managervacations.vacationsservice.gateway.VacationClient;
import com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.UserResponse;
import com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.holiday.HolidayExistResponse;
import com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.profile.UserAndProfileResponse;
import com.dharbor.talent.managervacations.vacationsservice.mapper.VacationStructMapper;
import com.dharbor.talent.managervacations.vacationsservice.service.IVacationService;
import com.dharbor.talent.managervacations.vacationsservice.service.VacationEventsService;
import com.dharbor.talent.managervacations.vacationsservice.util.Utils;
import com.dharbor.talent.managervacations.vacationsservice.validator.*;
import feign.FeignException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class CreateVacationUseCase {

    private IVacationService iVacationService;

    private VacationStructMapper vacationStructMapper;

    private SecurityClient securityClient;

    private VacationClient vacationClient;

    private Message message;

    private Verifications verifications;

    private VacationEventsService vacationEventsService;


    public VacationResponse execute(VacationRequest vacationRequest, Long userId) {
        VerifyDateBeforeDate(vacationRequest.getDayOff());
        VerifyExistDayOffSameDay(vacationRequest.getDayOff(), userId);
        VerifyDayOfWeek(vacationRequest.getDayOff());
        validateDaysTypeOfVacation(vacationRequest, userId);
        Long idCountry = securityClient.getProfileByUserId(userId).getProfile().getIdCountry();
        HolidayExistResponse existDate = vacationClient.getHolidaysByDateAndCountryId(vacationRequest.getDayOff(), idCountry);
        System.out.println(existDate);
        Vacation vacationBd = iVacationService.save(buildVacation(vacationRequest, userId));
        VacationDto vacationDto = vacationStructMapper.vacationToVacationDtio(vacationBd);
        vacationEventsService.publish(vacationDto);
        return new VacationResponse(vacationDto);
    }

    private Vacation buildVacation(VacationRequest vacationRequest, Long idUser) {
        Vacation newVacation = new Vacation();
        newVacation.setVacationType(vacationRequest.getVacationType());
        newVacation.setDayOff(vacationRequest.getDayOff());
        newVacation.setUserId(idUser);
        return newVacation;
    }

    private void VerifyDateBeforeDate(Date dateOff) {
        verifications.verify(new VerifyDateBeforeCommand(dateOff,
                message.getMessage("Vacation.Date.Not.Future")));
    }

    private void VerifyExistDayOffSameDay(Date dateOff, Long userId) {
        verifications.verify(new VerifyExistDayOffSameDayCommand(
                message.getMessage("Same.Date.Vacation"),
                iVacationService.existsByDayOffAndUserId(dateOff, userId)));
    }

    private void VerifyDayOfWeek(Date dateOff) {
        verifications.verify(new VerifyDayOfWeekCommand(dateOff,
                message.getMessage("Weekend.Date.Vacation")));
    }

    private void validateDaysTypeOfVacation(VacationRequest vacationRequest, Long userId) {
        switch (vacationRequest.getVacationType()) {
            case HOLIDAY:
                validateHolidays(userId);
                break;
            case COMPENSATION:
                validateCompensations(userId);
                break;
            default:
                break;
        }
    }

    private void validateHolidays(Long userId) {

        UserResponse userResponse = validateUserExist(userId);
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDateTime userDateOfIncorporation = Utils.convertDateToLocalDate(userResponse.getUserDto().getCreatedDate());
        long numberOfVacationDaysAvailable = (ChronoUnit.YEARS.between(userDateOfIncorporation, localDateTime) * 15)
                - iVacationService.countByUserIdAndVacationStatus(userId, VacationStatus.APPROVED);
        if (numberOfVacationDaysAvailable <= 0) {
            throw new BadRequestException(message.getMessage("Vacation.Date.Not.Available"));
        }
    }

    private void validateCompensations(Long userId) {
        UserResponse userResponse = validateUserExist(userId);
        UserAndProfileResponse userAndProfileResponse = securityClient.getProfileByUserId
                (userResponse.getUserDto().getId());
        verifications.verify(new VerifyDaysOffCommand(userAndProfileResponse.getProfile().getNumberOfOffsets(),
                message.getMessage("Compensation.Date.Not.Available")));
        securityClient.subtractNumberOfOffsets(userId);
    }


    private UserResponse validateUserExist(Long idUser) {
        try {
            return securityClient.getUserById(idUser);
        } catch (FeignException e) {
            if (e.getMessage().contains("1011") || e.getMessage().contains("1010")) {
                throw new ResourceNotFound(message.getMessage("Not.Exist.User.message"));
            } else {
                throw new ServerErrorException(message.getMessage("Problem.Server.Security.message"));
            }
        }
    }


}

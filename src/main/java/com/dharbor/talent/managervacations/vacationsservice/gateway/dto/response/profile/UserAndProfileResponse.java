package com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.profile;

import com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.CommonResponseDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserAndProfileResponse extends CommonResponseDto {

    private ProfileDto profile;

}

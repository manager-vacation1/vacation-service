package com.dharbor.talent.managervacations.vacationsservice.usecase;

import com.dharbor.talent.managervacations.vacationsservice.domain.Vacation;
import com.dharbor.talent.managervacations.vacationsservice.dto.VacationDto;
import com.dharbor.talent.managervacations.vacationsservice.dto.request.VacationUpdateRequest;
import com.dharbor.talent.managervacations.vacationsservice.dto.response.VacationResponse;
import com.dharbor.talent.managervacations.vacationsservice.mapper.VacationStructMapper;
import com.dharbor.talent.managervacations.vacationsservice.service.IVacationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class UpdateVacationStatusUseCase {

    private IVacationService iVacationService;

    private VacationStructMapper vacationStructMapper;

    public VacationResponse execute(VacationUpdateRequest vacationUpdateRequest) {
        Vacation vacation = iVacationService.findById(vacationUpdateRequest.getIdVacation()).get();
        vacation.setVacationStatus(vacationUpdateRequest.getVacationStatus());
        Vacation vacation1 = iVacationService.save(vacation);
        VacationDto vacationDto = vacationStructMapper.vacationToVacationDtio(vacation1);
        return new VacationResponse(vacationDto);
    }
}

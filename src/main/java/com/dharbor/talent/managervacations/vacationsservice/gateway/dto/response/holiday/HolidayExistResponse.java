package com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.holiday;

import com.dharbor.talent.managervacations.vacationsservice.gateway.dto.response.CommonResponseDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class HolidayExistResponse extends CommonResponseDto {
    private boolean holidayExist;
}

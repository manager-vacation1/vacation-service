package com.dharbor.talent.managervacations.vacationsservice.validator;

import com.dharbor.talent.managervacations.vacationsservice.exception.BadRequestException;
import com.dharbor.talent.managervacations.vacationsservice.util.Utils;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author Jhonatan Soto
 */
public class VerifyDayOfWeekCommand implements Command {

    private final String message;

    private final Date date;

    public VerifyDayOfWeekCommand(Date date, String message) {
        this.message = message;
        this.date = date;
    }

    @Override
    public void execute() {
        LocalDateTime localDateTime = Utils.convertDateToLocalDate(date);
        if (localDateTime.getDayOfWeek() == DayOfWeek.SATURDAY ||
                localDateTime.getDayOfWeek() == DayOfWeek.SUNDAY) {
            throw new BadRequestException(message);
        }

    }
}
